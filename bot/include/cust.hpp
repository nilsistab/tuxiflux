#pragma once
#include <string>
#include <unordered_map>
#include <cdlpp/bot.hpp>
#include <cdlpp/cdltypes-incomplete.hpp>
#include "database.hpp"


extern const std::string default_prefix;
extern std::map<uint64_t, std::string> prefix_cache;

void in_main();
void on_message(CDL::CMessage msg, std::function<void (CDL::CMessage)> cb);
void on_error(const std::string& message);
void get_prefix(CDL::CChannel channel, std::function<void (const std::string&)> cb);
bool is_globalchat(CDL::CChannel channel);
