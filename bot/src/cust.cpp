#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>
#include <cdlpp/cdltypes.hpp>
#include "database.hpp"
#include "db_templates.hpp"
#include "cust.hpp"
using namespace std;
using namespace CDL;

const string default_prefix = "t#";
std::map<uint64_t, std::string> prefix_cache;


static const std::vector<std::string> get_table_tmpl(db_templates::type type) {
    using namespace db_templates;
    switch (type) {
        case guild:
            return {"PREFIX          TEXT                              ",
                    "WELCOMEDM       TEXT                              ",
                    "AUTOROLES       TEXT                              "};
        case user:
            return {"BALANCE         INT CHECK(BALANCE > -1)           ",
                    "LAST_DAILY      INT                               ",
                    "GC_BLACKLISTED  BOOLEAN                           "};
        case member:
            return {"WARN0           TEXT                              ",
                    "WARN1           TEXT                              ",
                    "WARN2           TEXT                              ",
                    "WARNS           INT                               "};
    }
    return {};
}
static const std::map<std::string, std::string> get_record_tmpl(db_templates::type type) {
    using namespace db_templates;
    switch (type) {
        case guild:
            return {
                       {"PREFIX", default_prefix},
                       {"WELCOMEDM", ""},
                       {"AUTOROLES", ""}
                   };
        case user:
            return {
                       {"BALANCE", env.settings.contains("start_money")?std::to_string(env.settings["start_money"].get<uint32_t>()):"0"},
                       {"LAST_DAILY", "-1"},
                       {"GC_BLACKLISTED", "false"},
                   };
        case member:
            return  {
                {"WARN0", ""},
                {"WARN1", ""},
                {"WARN2", ""},
                {"WARNS", "0"},
            };
    }
    return {};
}

void in_main() {
    // Initialise database client
    env.db = new Database(*env.aioc, get_table_tmpl, get_record_tmpl);
}

void get_prefix(CChannel channel, std::function<void (const std::string&)> cb) {
    auto server_id = channel->guild_id;
    if (not server_id) {
        cb(default_prefix);
    } else {
        // Try cache
        auto res = prefix_cache.find(server_id);
        if (res != prefix_cache.end()) {
            // Cache entry found, return
            cb(res->second);
        } else {
            // Cache entry not found, fetch and create
            env.db->get<std::string>(to_dbid(server_id), "prefix", [=] (auto prefix) {
                prefix_cache[server_id] = prefix;
                cb(prefix);
            });
        }
    }
}

bool is_globalchat(CChannel channel) {
    return channel->type == ChannelTypes::GUILD_TEXT and
           channel->name.find("tuxiflux-globalchat") != std::string::npos;
}

void on_message(CMessage msg, std::function<void (CMessage)> cb) {
    msg->get_channel([msg, cb] (CChannel channel) {
        // Check if current channel is on a server
        if (channel->type != ChannelTypes::GUILD_TEXT) {
            cb(msg);
            return;
        }
        msg->get_guild([msg, channel, cb] (CGuild server) {
            // Abort if current chat isn't a globalchat
            if (not is_globalchat(channel)) {
                cb(msg);
                return;
            }
            // Check if message has no content
            if (msg->content.empty()) {
                msg->remove();
                return;
            }
            // Check if user is blacklisted
            env.db->get<bool>(to_dbid(msg->author->id), "GC_BLACKLISTED", [=] (auto blacklisted) {
                if (blacklisted) {
                    msg->remove();
                    return;
                }
                // Generate embed
                nlohmann::json embed;
                {
                    auto &user = msg->author;
                    embed["description"] = msg->content;
                    embed["author"]["name"] = user->get_full_name();
                    embed["author"]["icon_url"] = extras::get_avatar_url(user);
                    embed["footer"]["text"] = "Server • "+server->name+" | ID • "+to_string(user->id);
                    // Get color
                    uint32_t color = 0x696969; // Grey
                    for (const auto& teammember : env.settings["team"].items()) {
                        if (teammember.value() == user->id) {
                            // User is team member
                            color = 0x40e0d0; // Turquoise
                        }
                    }
                    embed["color"] = color;
                }
                // Delete original message
                msg->remove();
                // Distribute message around servers
                for (const auto& [guild_id, guild] : cache::guild_cache) {
                    for (const auto& [channel_id, channel] : guild->channels) {
                        if (is_globalchat(channel)) {
                            channel->send_embed(embed);
                        }
                    }
                }
            }, db_templates::user);
        });
    });
}

void on_error(const std::string& message) {
    if (env.settings.contains("error_report_channel")) {
        fetch::channel(env.settings["error_report_channel"], [message] (CChannel channel) {
            if (channel) {
                channel->send("An unhandled exception has occured:\n"
                              "```\n"
                              +message+
                              "\n```");
            } else {
                std::cerr << "Couldn't find error report channel" << std::endl;
            }
        });
    }
}

int main(int argc, char **argv) {
    // Set handlers
    handlers::get_prefix = get_prefix;
    handlers::in_main = in_main;
    handlers::on_message = on_message;
    handlers::on_error = on_error;

    // Set intents
    CDL::intents::ready.push_back([] () {
         auto scroller = new PresenceScroller();
         scroller->presences.push_back([] () {
             Presence fres;
             fres.activities.push_back(Activity(
                 "on "+std::to_string(cache::guild_cache.size())+" servers",
                 ActivityType::playing
             ));
             return fres;
         });
         scroller->presences.push_back([] () {
             Presence fres;
             fres.activities.push_back(Activity(
                 std::to_string(cache::user_cache.size())+" users",
                 ActivityType::listening
             ));
             return fres;
         });
    });

    // Set cache props
    cache::ctrl::message = false;

    // Run
    using namespace intent_vals;
    CDL::main(argc, argv, GUILD_MESSAGES | DIRECT_MESSAGES | GUILDS | GUILD_BANS | GUILD_EMOJIS | GUILD_MEMBERS | GUILD_BANS);
}
