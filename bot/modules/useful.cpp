#include <string>
#include <ostream>
#include <array> // TODO: remove when no longer needed
#include <cdlpp/cdltypes.hpp>
#include "cust.hpp"
#include "generic_msgs.h"
#include "help.hpp"
#include "permassert.hpp"
using namespace std;
using namespace CDL;



class Botinfo {
    static void serverinfo_roles(CChannel channel) {
        channel->get_guild([channel] (CGuild server) {
            // Get info text
            ostringstream info_text;
            info_text << "**Roles** (**" << server->roles.size() << "**)\n• ";
            for (const auto& [id, role] : server->roles) {
                info_text << role->get_mention() << ", ";
            }
            // Send embed
            json embed = {
                {"description", info_text.str()}
            };
            channel->send_embed(embed);
        });
    }

    static void serverinfo_emojis(CChannel channel) {
        channel->get_guild([channel] (CGuild server) {
            // Get info text
            ostringstream info_text;
            info_text << "**Emojis** (**" << server->emojis.size() << "**)\n• ";
            for (const auto& [id, emoji] : server->emojis) {
                info_text << emoji->get_mention() << ", ";
            }
            // Send embed
            json embed = {
                {"description", info_text.str()}
            };
            channel->send_embed(embed);
        });
    }

    static void serverinfo(CMessage, CChannel channel, CDL::cmdargs& args) {
        gchannelassert(channel);

        // Check arguments
        if (not args.empty()) {
            auto mode = args[0];
            // Find
            if (mode == "roles") {
                serverinfo_roles(channel);
            } else if (mode == "emojis") {
                serverinfo_emojis(channel);
            } else {
                send_badusage(channel, "serverinfo");
            }
            return;
        }

        // Get infos
        auto server = channel->get_guild();
        server->get_owner([args, channel, server] (CUser server_owner) {
            server->get_bans([args, channel, server, server_owner] (const std::unordered_map<uint64_t, Ban*>& server_bans) {
                ostringstream server_features;
                auto server_avatar_url = extras::get_avatar_url(server);
                auto server_creation_date = "not yet implemented";
                size_t server_members = 0,
                       server_bots = 0,
                       server_humans = 0,
                       server_textchannels = 0,
                       server_voicechannels = 0,
                       server_categories = 0;

                // Count channels
                for (const auto& [id, channel] : server->channels) {
                    switch (channel->type) {
                        using namespace ChannelTypes;
                        case GUILD_TEXT: server_textchannels++; break;
                        case GUILD_VOICE: server_voicechannels++; break;
                        case GUILD_CATEGORY: server_categories++; break;
                        default: break;
                    }
                }

                // Count members
                for (const auto& [id, member] : server->members) {
                    server_members++;
                    CUser user = member->lazy_user;
                    if (user?user->bot:false) {
                        server_bots++;
                    } else {
                        server_humans++;
                    }
                }

                // Generate server features list string
                if (server->features.size() == 0) {
                    server_features << "**none**";
                } else {
                    for (auto feature : server->features) {
                        server_features << feature_get_nice(feature) << ", ";
                    }
                }
                auto server_featuresstr = server_features.str();

                // Get info text
                get_prefix(channel, [=] (const std::string& prefix) {
                    ostringstream info_text;
                    info_text << "**Owner**\n"
                                 "• " << server_owner->get_full_name() << " (`" << server_owner->id << "`)\n"
                                 "\n"
                                 "**Server region**\n"
                                 "• " << server->region << "\n"
                                 "\n"
                                 "**Members**\n"
                                 "• **" << server_members << "** members, thereof **" << server_bots << "** bots and **" << server_humans << "** humans\n"
                                 "• **" << server_bans.size() << "** users banned\n"
                                 "\n"
                                 "**Channels**\n"
                                 "• **" << server_textchannels << "** text channels\n"
                                 "• **" << server_voicechannels << "** voice channels\n"
                                 "• **" << server_categories << "** categories\n"
                                 "\n"
                                 "**Serverboost**\n"
                                 "• Level: **" << server->premium_tier << "**\n"
                                 "• Boosts: **" << server->premium_subscription_count << "**\n"
                                 "\n"
                                 "**Features**\n"
                                 "• " << server_featuresstr << "\n"
                                 "\n"
                                 "**Server created**\n"
                                 "• " << server_creation_date << "\n"
                                 "\n"
                                 "**Emojis**\n"
                                 "• Please use `" << prefix << "serverinfo emojis`\n"
                                 "\n"
                                 "**Role**\n"
                                 "• Please use `" << prefix << "serverinfo roles`";

                    // Send info text
                    json embed = {
                        {"title", server->name + " (" + to_string(server->id) + ')'},
                        {"description", info_text.str()},
                    };
                    if (not server_avatar_url.empty()) {
                        embed["thumbnail"]["url"] = server_avatar_url;
                    }
                    channel->send_embed(embed);
                });
            });
        });
    }

    static void avatar(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        auto server = channel->get_guild();
        // Define another function via lambda (which is ugly lol)
        auto fnc = [args, channel] (CUser user) {
            // Check user
            if (not user) {
                channel->send(NO_SUCH_USER);
                return;
            }
            // Get avatar URL
            string avatar_url = extras::get_avatar_url(user);
            // Create embed
            json embed = {
                {"description", ":frame_photo: "+user->username+"'s Avatar | [Link]("+avatar_url+")"},
                {"image", {{"url", avatar_url}}}
            };
            // Send embed
            channel->send_embed(embed);
        };
        // Get user
        if (args.empty()) {
            fnc(msg->author);
        } else {
            fetch::user(extras::find_user_id(server, args[0]), fnc);
        }
    }


public:
    Botinfo() {
        using namespace CDL;
        // Commands
        register_command("serverinfo", serverinfo, 1);
        register_command("avatar", avatar, 1);
    }
};




static Botinfo botinfo;
