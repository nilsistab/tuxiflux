#include <string>
#include <vector>
#include <random>
#include <cdlpp/cdltypes.hpp>
#include "cust.hpp"
#include "database.hpp"
#include "generic_msgs.h"
#include "help.hpp"
using namespace std;
using namespace CDL;


class Fun {
    static void random(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        // Check arguments
        if (args.size() != 2) {
            send_badusage(channel, "random");
            return;
        }
        // Get numbers
        int min, max;
        try {
            min = stoi(args[0]);
            max = stoi(args[1]);
        } catch (exception&) {
            channel->send(":warning: **Both arguments** have to be numbers in range!");
            return;
        }
        // Check numbers
        if (min > max) {
            // Swap numbers if they are the wrong way around
            swap(min, max);
        }
        // Initialise generator
        mt19937 gen(msg->id);
        uniform_int_distribution<> distr(min, max);
        // Send result
        auto rndno = distr(gen);
        channel->send(":game_die: **"+to_string(rndno)+"**");
    }

    static void eightball(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        static const vector<string> answers = {"It is certain", "Without a doubt", "You may rely on it", "Yes definitely", "It is decidedly so", "As I see it, yes", "Most likely", "Yes",  "Outlook good", "Signs point to yes",
                                               "Reply hazy try again", "Better not tell you now", "Ask again later", "Cannot predict now", "Concentrate and ask again",
                                               "Don’t count on it", "Outlook not so good", "My sources say no", "Very doubtful", "My reply is no"
                                              };
        // Check arguments
        if (args.size() == 0) {
            send_badusage(channel, "8ball");
            return;
        }
        // Send answer
        channel->send(":crystal_ball: "+answers[msg->id % (answers.size() - 1)]);
    }



public:
    Fun() {
        using namespace CDL;
        // Commands
        register_command("random", random, 2);
        register_command("8ball", eightball, 1);
    }
};




static Fun fun;
