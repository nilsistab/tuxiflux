#include <string>
#include <vector>
#include <ostream>
#include <chrono>
#include <exception>
#include "cust.hpp"
#include "help.hpp"
#include "generic_msgs.h"
#include "permassert.hpp"
#include <cdlpp/cdltypes.hpp>
using namespace std;
using namespace CDL;


class Misc {
    using timer = std::chrono::system_clock;
    timer::time_point startup_time;


    static void invite(CMessage, CChannel channel, CDL::cmdargs&) {
        json embed = {
            {"description", ":speech_balloon: You can invite me using [this]("+extras::get_bot_invite_link()+") link"},
            {"color", 0xffffff}
        };
        channel->send_embed(embed);
    }

    static void setup(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        gchannelassert(channel);
        // Check arguments
        if (args.empty()) {
            json embed = {
                {"description", ":speech_balloon: I will set up the stuff for your server if you choose one of the following arguments.\n"
                                "\n"
                                "• `globalchat`: I create a global chat and set the permissions for the channel accordingly.\n"
                }
            };
            channel->send_embed(embed);
        } else if (args[0] == "globalchat") {
            permassert(msg, channel, Permissions::MANAGE_CHANNELS);
            auto server = channel->get_guild();
            // Check if channel already exists
            for (const auto& [rchannel_id, rchannel] : server->channels) {
                if (is_globalchat(rchannel)) {
                    channel->send(":warning: Look, there already is one: "+rchannel->get_mention());
                    return;
                }
            }
            // Create permission overwrites
            vector<PermissionOverwrite> poverwrites = {
                // TODO
            };
            // Create channel
            Channel newchannel({});
            {
                newchannel.name = "tuxiflux-globalchat";
                using namespace Permissions;
                newchannel.overwrites[env.self->id] = {
                    env.self->id,
                    PermissionOverwrite::member,
                    VIEW_CHANNEL | SEND_MESSAGES | MANAGE_MESSAGES,
                    0
                };
            }
            server->create_channel(newchannel, [channel] (CChannel newchannel) {
                if (newchannel) {
                    channel->send(":speech_balloon: "+newchannel->get_mention()+" was created and set up. Have fun!");
                } else {
                    channel->send(":warning: Channel creation failed. Do I have enough permissions?");
                }
            });
        } // else if (args[0] == "xxx")
    }

    void about(CMessage , CChannel channel, CDL::cmdargs&) {
        // Get Uptime in hours
        timer::duration uptime = timer::now() - startup_time;
        auto uptime_hours = uptime.count() / 1000000000 / 60 / 60;
        // Get prefix
        get_prefix(channel, [=] (const std::string& prefix) {
            // Get mem usage string
            ostringstream mem_use;
    #       ifdef extras_get_mem_supported
                mem_use << "Memory usage • **" << extras::get_mem::used() << "** MB / **" << extras::get_mem::total() << "** MB\n";
    #       endif
            // Build info text
            ostringstream text;
            text << "Online since • **" << uptime_hours << "** hours\n"
                    "\n" <<
                    mem_use.str() <<
                    "C++ version • **" << __cplusplus << "**\n"
                    "Boost version • **" BOOST_LIB_VERSION "**\n"
                    "Compiler • **" COMPILER_ID " " COMPILER_VERSION " (" COMPILER_PLATFORM ")**\n"
                    "\n"
                    "Servers • **" << cache::guild_cache.size() << "**\n"
                    "\n"
                    "Prefix in chat • `" << prefix << '`';
            // Create embed
            json embed = {
                {"description", text.str()},
                {"thumbnail", {{"url", extras::get_avatar_url(env.self)}}}
            };
            // Send embed
            channel->send_embed(embed);
        });
    }

    static void gc_autorules(CChannel channel) {
        if (is_globalchat(channel)) {
            channel->send_embed({
                                    {"title", "**IMPORTANT**"},
                                    {"description",
                                     "**Rules**\n"
                                     "\n"
                                     "**1.** English only\n"
                                     "**2.** Common sense\n"
                                     "**3.** No spam\n"
                                     "**4.** No advertisement\n"
                                     "**5.** No bot support"
                                    }
                                }, "", [] (CMessage msg) {
                if (msg) msg->pin();
            });
        }
    }

    static void gc_blacklist(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        teamassert(msg);
        // Check args
        if (args.size() < 2) {
            send_badusage(channel, "gc_blacklist");
            return;
        }
        // Get boolean value
        bool newval;
        if (args[0] == "add") {
            newval = true;
        } else if (args[0] == "remove") {
            newval = false;
        } else {
            send_badusage(channel, "gc_blacklist");
            return;
        }
        // Find user
        uint64_t user_id = extras::find_user_id(channel->get_guild(), args[1]);
        if (not user_id) {
            channel->send(NO_SUCH_USER);
            return;
        }
        // Set blacklisted boolean
        env.db->update(to_dbid(user_id), "GC_BLACKLISTED", newval, nullptr, db_templates::user);
    }

    static void restart(CMessage msg, CChannel channel, CDL::cmdargs&) {
        teamassert(msg);
        channel->send("Restarting...", [] (CMessage ) {
            CDL::restart_bot();
        });
    }
    static void reload(CMessage msg, CChannel channel, CDL::cmdargs&) {
        teamassert(msg);
        try {
            CDL::reload_config();
        } catch (std::exception& e) {
            channel->send("Error reloading config: "+std::string(e.what()));
            return;
        }
        channel->send("Config reloaded successfully");
    }



public:
    Misc() {
        using namespace CDL;
        startup_time = timer::now();
        // Commands
        register_command("invite", invite, NO_ARGS);
        register_command("setup", setup, 1);
        register_command("gc_blacklist", gc_blacklist, 2);
        register_command("gc_ban", gc_blacklist, 2);
        register_command("restart", restart, NO_ARGS);
        register_command("reload", reload, NO_ARGS);
        cdl_register_nonstatic_command("about", about, NO_ARGS);
        // Events
        intents::channel_create.push_back(gc_autorules);
    }
};




static Misc misc;
