#include <ostream>
#include <exception>
#include <ctime>
#include <cdlpp/cdltypes.hpp>
#include "cust.hpp"
#include "database.hpp"
#include "generic_msgs.h"
#include "help.hpp"
using namespace std;
using namespace CDL;


class Money {
#   define mod_money(user_id, cb, amount) env.db->update(to_dbid(user_id), "BALANCE = BALANCE + ("+amount+")", cb, db_templates::user)
#   define get_money(user_id, cb) env.db->get<int>(to_dbid(user_id), "BALANCE", cb, db_templates::user)


    static void balance(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        // Get target user
        uint64_t user_id;
        if (args.empty()) {
            user_id = msg->author->id;
        } else {
            user_id = extras::find_user_id(channel->get_guild(), args[0]);
            if (not user_id) {
                channel->send(NO_SUCH_USER);
                return;
            }
        }
        // Check if target user == bot
        if (user_id == env.self->id) {
            channel->send(":warning: Sadly, I am not allowed to have money :confused:");
            return;
        }
        fetch::user(user_id, [channel] (CUser user) {
            if (not user) return;
            // Get values
            get_money(user->id, [=] (auto amount) {
                // Send
                channel->send("**"+user->get_full_name()+"** currently has **"+to_string(amount)+"** :dollar:");
            });
        });
    }

    static void daily(CMessage msg, CChannel channel, CDL::cmdargs&) {
        // Get current time
        time_t time_raw;
        time(&time_raw);
        tm *time = gmtime(&time_raw);
        // Get last day command was executed
        env.db->get<int>(to_dbid(msg->author->id), "LAST_DAILY", [=] (auto last_daily) {
            // Verify that next day was reached
            if (last_daily == time->tm_yday) {
                channel->send(":warning: You can't get your daily money twice a day\n"
                              ":information_source: Please note that I'm running in GMT :wink:");
                return;
            }
            // Update database
            int32_t daily_money = env.settings["daily_money"];
            env.db->update(to_dbid(msg->author->id), "LAST_DAILY", time->tm_yday, nullptr, db_templates::user);
            mod_money(msg->author->id, [=] (const bool error) {
                if (error) {

                } else {
                    // Get new balance
                    get_money(msg->author->id, [=] (auto new_value) {
                        auto old_value = new_value - daily_money;
                        // Send result
                        channel->send(":dollar: You've received your daily money!\n"
                                      "**"+to_string(old_value)+"** + **"+to_string(daily_money)+"** = **"+to_string(new_value)+"**");
                    });
                }
            }, to_string(daily_money));
        }, db_templates::user);
    }

    static void pay(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        // Get target user
        uint64_t target_user_id;
        if (args.size() != 2) {
            send_badusage(channel, "pay");
            return;
        } else {
            target_user_id = extras::find_user_id(channel->get_guild(), args[0]);
            if (not target_user_id) {
                channel->send(NO_SUCH_USER);
                return;
            }
        }
        // Check target user
        if (target_user_id == env.self->id) {
            channel->send(":warning: Why do you want to give me money?");
            return;
        } else if (target_user_id == msg->author->id) {
            channel->send(":warning: That's yourself lol");
            return;
        }
        // Check if specified user ID actually exists and is fetchable
        string &amount_str = args[1];
        fetch::user(target_user_id, [msg, channel, amount_str] (CUser target_user) {
            if (not target_user) {
                channel->send(NO_SUCH_USER);
                return;
            }
            // Check if number is clean
            if (not extras::is_digits(amount_str)) {
                channel->send(":warning: The amount of money you've specified isn't valid :thinking:");
                return;
            }
            // Pay out
            mod_money(msg->author->id, [=] (const bool error) {
                if (error) {
                    channel->send(":warning: I wasn't able to pay out... Are you sure you've got enough money?");
                } else {
                    mod_money(target_user->id, nullptr, amount_str);
                    // Report success
                    channel->send("You've sent **"+amount_str+"** :dollar: to **"+target_user->get_full_name()+"**");
                }
            }, string("-")+amount_str);
        });
    }

    static void bet(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        // Check args
        if (args.size() != 1 or not extras::is_digits(args[0])) {
            send_badusage(channel, "bet");
            return;
        }
        int32_t insert = stoi(args[0]);
        auto insert_str = to_string(insert);
        // Check if insert is in range
        if (not (insert != 0 and insert < 100)){
            channel->send(":warning: **"+insert_str+"** is not > **0** and < **100**!");
            return;
        }
        // Check if user has enough money
        get_money(msg->author->id, [=] (auto amount) {
            if (amount < insert) {
                channel->send(":warning: Excuse me, but you don't have enough money to do that!");
                return;
            }
            // Get "random" value
            bool lost = msg->id % 4 == 0;
            if (insert > 10) {
                lost =! lost;
            }
            // Do database transaction
            mod_money(msg->author->id, nullptr, std::string(lost?"-":"")+insert_str);
            // Report value of won
            if (lost) {
                channel->send(":thumbsdown: You've lost **"+insert_str+"** :dollar:");
            } else {
                channel->send(":thumbsup: You've won **"+insert_str+"** :dollar:");
            }
        });
    }



public:
    Money() {
        using namespace CDL;
        // Commands
        register_command("balance", balance, 1);
        register_command("bal", balance, 1);
        register_command("pay", pay, 2);
        register_command("bet", bet, 1);
        register_module_initialiser([] () {
            if (env.settings.contains("daily_money")) {
                register_command("daily", daily, NO_ARGS);
            }
        });
    }
};




static Money money;
