#include <fmt/format.h>
#include <cdlpp/cdltypes.hpp>
#include "database.hpp"
#include "permassert.hpp"
#include "help.hpp"
#include "cust.hpp"
#include "generic_msgs.h"
using namespace std;
using namespace CDL;


class Basic {
    static void prefix(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        gchannelassert(channel);
        auto server = channel->get_guild();
        // Check arguments
        if (args.empty()) {
            // Get prefix
            get_prefix(channel, [=] (const std::string& prefix) {
                channel->send(":paperclips: The individual prefix for **"+server->name+"** is `"+prefix+'`');
            });
            return;
        }
        // Check permissions
        permassert(msg, channel, Permissions::ADMINISTRATOR);
        // Get new prefix
        string &newprefix = args[0];
        bool reset = newprefix == "reset";
        if (reset) {
            newprefix = default_prefix;
        }
        // Perform database operation
        env.db->update(to_dbid(server->id), "prefix", newprefix);
        // Update cache
        prefix_cache[server->id] = newprefix;
        // Inform about success
        if (reset) {
            channel->send(":paperclips: Your individual prefix has been reset for your server!");
        } else {
            channel->send(":paperclips: Your individual prefix for your server was changed to **"+newprefix+"**.");
        }
    }

    inline static string welcomedm_format(CGuild guild, CUser user, const string& message) {
        return fmt::format(message, user->username, guild->name);
    }
    static void welcomedm_get(CGuild guild, CUser user, std::function<void (const std::string& str)> cb) {
        env.db->get<string>(to_dbid(guild->id), "WELCOMEDM", [=] (auto welcomedm_str) {
            if (not (welcomedm_str.empty() or welcomedm_str == "none")) {
                cb(welcomedm_format(guild, user, welcomedm_str));
            } else {
                cb("none");
            }
        }, db_templates::guild);
    }
    inline static void welcomedm_set(CGuild guild, const string& message) {
        env.db->update(to_dbid(guild->id), "WELCOMEDM", message, nullptr, db_templates::guild);
    }
    inline static void welcomedm_send(CMember member) {
        member->get_user([member] (CUser user) {
            welcomedm_get(member->guild, user, [=] (const std::string& welcomedm_msg) {
                if (welcomedm_msg != "none") {
                    user->get_dm([welcomedm_msg] (CChannel channel) {
                        if (not channel) return;
                        channel->send(welcomedm_msg);
                    });
                }
            });
        });
    }
    static void welcomedm(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        gchannelassert(channel);
        permassert(msg, channel, Permissions::ADMINISTRATOR);
        // Check args
        if (args.empty()) {
            // Return current welcomedm
            welcomedm_get(channel->get_guild(), msg->author, [=] (const std::string& welcomedm_msg) {
                channel->send(welcomedm_msg);
            });
        } else {
            // Set welcomedm
            string &newmsg = args[0];
            try {
                auto server = channel->get_guild();
                channel->send_embed({
                                        {"title", "The new welcome DM is"},
                                        {"description", welcomedm_format(server, msg->author, newmsg)}
                                    });
                welcomedm_set(server, newmsg);
            }  catch (fmt::format_error& e) {
                channel->send(":warning: Failed to set welcome DM: "+string(e.what()));
            }
        }
    }

    static void banorkick(CMessage msg, CChannel channel, CDL::cmdargs& args, bool permanent) {
        // Check permissions
        gchannelassert(channel);
        permassert(msg, channel, permanent?Permissions::BAN_MEMBERS:Permissions::KICK_MEMBERS);
        // Check arguments
        if (args.size() < 2) {
            send_badusage(channel, permanent?"ban":"kick");
            return;
        }
        // Get ID
        auto id_int = extras::find_user_id(channel->get_guild(), args[0]);
        auto id_str = to_string(id_int);
        // Check for error
        if (not id_int) {
            channel->send(NO_SUCH_USER);
            return;
        }
        // Try to ban
        auto reason = args[1];
        auto cb = [channel, id_str, id_int, reason, permanent] (const bool error) {
            if (error) {
                channel->send(":warning: That didn't work. Please make sure I have the permission to ban (Use the `setup check` command) "
                              "and that the user you've specified actually exists.");
            } else {
                // If user is in cache, show its full name; else mention
                auto user = cache::get_user(id_int);
                auto identifier = user?user->get_full_name():"<@"+id_str+">";
                channel->send(":smiling_imp: **"+identifier+"** was "+string(permanent?"banned":"kicked")+". (**"+reason+"**)");
            }
        };
        if (permanent) {
            channel->get_guild()->ban(id_int, reason, 7, cb);
        } else {
            channel->get_guild()->kick(id_int, reason, cb);
        }
    }
    static void ban(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        banorkick(msg, channel, args, true);
    }
    static void kick(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        banorkick(msg, channel, args, false);
    }

    static void nick(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        gchannelassert(channel);
        auto server = channel->get_guild();
        uint64_t user_id;
        // Get target member
        if (args.empty()) {
            user_id = msg->author->id;
            args.push_back(msg->author->username);
        } else {
            user_id = extras::find_user_id(server, args[0]);
        }
        // Check if get or set
        if (args.size() != 2) {
            // Check if target member exists
            auto res = server->members.find(user_id);
            if (res == server->members.end()) {
                channel->send(NO_SUCH_USER);
                return;
            } else {
                if (res->second->nick.empty()) {
                    channel->send(":information_source: **"+args[0]+"** does not have a nickname");
                } else {
                    channel->send(":information_source: The nickname of **"+args[0]+"** is **"+res->second->nick+"**");
                }
            }
        } else {
            // Check permissions
            if (user_id == msg->author->id) {
                permassert(msg, channel, Permissions::CHANGE_NICKNAME);
            } else {
                permassert(msg, channel, Permissions::MANAGE_NICKNAMES);
            }
            string &newnick = args[1];
            if (newnick == "reset") {
                newnick = "";
            }
            // Perform change
            channel->get_guild()->set_nick(user_id, newnick, [args, newnick, channel] (const bool error) {
                if (error) {
                    channel->send(":warning: I couldn't change their nickname.\n"
                                  ":information_source: Do I have enough permissions? Is the nick even valid?");
                } else {
                    if (newnick.empty()) {
                        channel->send(":information_source: The nickname of **"+args[0]+"** was removed.");
                    } else {
                        channel->send(":information_source: The nickname of **"+args[0]+"** was changed to **"+newnick+"**");
                    }
                }
            });
        }
    }

    static std::vector<Role*> autoroles_get(CGuild guild, const std::string& autoroles_str) {
        std::vector<Role*> fres;
        if (not autoroles_str.empty()) {
            auto autorole_strs = extras::strsplit(autoroles_str, ',');
            for (const auto& autorole_str : autorole_strs) {
                auto res = guild->roles.find(std::stoul(autorole_str));
                if (res != guild->roles.end()) {
                    if (not res->second->managed) {
                        fres.push_back(res->second);
                    }
                }
            }
        }
        return fres;
    }
    static void autoroles_assign(CMember member) {
        env.db->get<string>(to_dbid(member->guild->id), "AUTOROLES", [=] (auto autoroles_str) {
            // Parse and assign them
            auto autoroles = autoroles_get(member->guild, autoroles_str);
            if (not autoroles.empty()) {
                for (const auto& autorole : autoroles) {
                    member->roles.push_back(autorole->id);
                }
                member->commit();
            }
        });
    }
    static void autoroles(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        gchannelassert(channel);
        permassert(msg, channel, Permissions::MANAGE_ROLES);
        auto guild = channel->get_guild();
        // Check args
        if (args.empty()) {
            // Send current autoroles
            env.db->get<string>(to_dbid(guild->id), "AUTOROLES", [=] (auto autoroles_str) {
                auto autoroles = autoroles_get(guild, autoroles_str);
                std::ostringstream autoroles_strs;
                if (autoroles.empty()) {
                    autoroles_strs << "**none**";
                } else {
                    for (const auto& autorole : autoroles) {
                        autoroles_strs << autorole->get_mention() << ", ";
                    }
                }
                channel->send_embed({
                                        {"title", "Current autoroles"},
                                        {"description", autoroles_strs.str()}
                                    });
            });
        } else {
            // Set autoroles
            std::string autoroles_str;
            {
                // Compose string
                std::ostringstream autoroles_strs;
                for (const auto& arg : args) {
                    autoroles_strs << arg << ',';
                }
                autoroles_str = autoroles_strs.str();
                autoroles_str.pop_back();
            }
            // Check
            std::vector<Role*> autoroles;
            try {
                autoroles = autoroles_get(guild, autoroles_str);
            } catch (std::invalid_argument&) {}
            if (autoroles.size() != args.size()) {
                channel->send(":warning: Not all roles could be located");
            } else {
                env.db->update(to_dbid(guild->id), "AUTOROLES", autoroles_str);
                channel->send("Alright, done!");
            }
        }
    }


public:
    Basic() {
        using namespace CDL;
        // Commands
        register_command("prefix", prefix, 1);
        register_command("welcomedm", welcomedm, 1);
        register_command("ban", ban, 2);
        register_command("kick", kick, 2);
        register_command("nick", nick, 2);
        register_command("autoroles", autoroles, INF_ARGS);
        // Events
        intents::guild_member_add.push_back(welcomedm_send);
        intents::guild_member_add.push_back(autoroles_assign);
    }
};




static Basic basic;
