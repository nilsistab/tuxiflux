#include <string>
#include <ostream>
#include <fstream>
#include <fmt/format.h>
#include <cdlpp/extras.hpp>
#include <cdlpp/cdltypes.hpp>
#include "cust.hpp"
#include "help_pages.hpp"
using namespace std;
using namespace CDL;

static json syntaxes = json::parse(help_files.find("syntaxes")->second);



void send_badusage(CChannel channel, const string& command) {
    // Check if any usable syntax is listed to command
    if (not syntaxes.contains(command)) {
        channel->send(":warning: Incorrect command usage");
        return;
    }
    auto syntax = syntaxes[command];
    // Get prefix
    get_prefix(channel, [=] (const std::string& prefix) {
        // Check if badusage text exists
        if (syntax.contains("badusage_text")) {
            channel->send(fmt::format(string(syntax["badusage_text"]), prefix+command, string(syntax["usage"])));
        }
        // Alternatively send usage
        else {
            string x = fmt::format(":warning: Syntax: `{0} {1}`", prefix+command, string(syntax["usage"]));
            channel->send(x);
        }
    });
}


class Help {
    static void help(CMessage msg, CChannel channel, CDL::cmdargs& args) {
        // Get prefix
        get_prefix(channel, [=] (const std::string& prefix) {
            string help_name;
            string helpfile_name;
            bool help_mainpage = true;
            bool not_found = false;
            bool from_template = false;
            // If no arguments given, default help page
            if (args.empty()) {
                help_name = "1";
            } else {
                help_name = args[0];
            }
            // The name of the file might be equal to the name of the page passed
            helpfile_name = help_name;
            send_help:
            // Check if help page is a main page
            help_mainpage = extras::is_digits(helpfile_name);
            // Find given help page
            auto help_file = help_files.find("help_"+helpfile_name);
            if (help_file == help_files.end()) {
                // Check if notfound page exists
                if (not_found) {
                    // Error page couldn't be found
                    channel->send(":warning: Error page couldn't be found");
                    return;
                }
                // Check if syntax is listed
                if (syntaxes.contains(help_name)) {
                    helpfile_name = "syntaxtmpl";
                    from_template = true;
                    goto send_help;
                }
                // Send notfound page
                helpfile_name = "notfound";
                not_found = true;
                goto send_help;
            }
            const string &help_text = help_file->second;
            // Send text
            json embed;
            {
                // Formats in 2 different ways depending on the value of not_foud
                if (not_found) {
                    // Page not found
                    string syntax_list;
                    // Build list of syntaxes string
                    if (not syntaxes.empty()) {
                        ostringstream syntax_list_builder;
                        syntax_list_builder << "__Command syntax pages available__\n•";
                        for (const auto& [name, value] : syntaxes.items()) {
                            if (not (value.contains("unlisted") and value["unlisted"].get<bool>())) {
                                syntax_list_builder << " `" << string(name) << "`,";
                            }
                        }
                        syntax_list = syntax_list_builder.str();
                        syntax_list.pop_back(); // Remove trailing ','
                    }
                    // Generate list of available syntax pages
                    embed["description"] = fmt::format(help_text, help_name, syntax_list);
                } else {
                    // Check if templating is enabled
                    if (from_template) {
                        ostringstream examples;
                        auto syntax = syntaxes[help_name];
                        // Build examples string
                        if (syntax.contains("examples")) {
                            examples << "**Examples:**\n";
                            for (const auto& example : syntax["examples"].items()) {
                                examples << fmt::format("• `{0}{1} {2}`\n", prefix, help_name, string(example.value()));
                            }
                        }
                        // Apply template
                        embed["description"] = fmt::format(help_text, help_name, string(syntax["usage"]), examples.str());
                    } else if (help_mainpage) {
                        embed["description"] = fmt::format(help_text, prefix, extras::get_bot_invite_link(), string(env.settings["supportdiscord"]));
                    } else {
                        embed["description"] = help_text;
                    }
                }
                // The rest of the embed can differ too
                if (help_mainpage) {
                    embed["thumbnail"]["url"] = extras::get_avatar_url(env.self);
                    embed["footer"]["icon_url"] = extras::get_avatar_url(msg->author);
                    embed["footer"]["text"] = "Use "+prefix+"help <command> to see its syntax";
                    embed["color"] = 0x0000ff; // Blue
                } else {
                    embed["thumbnail"]["url"] = extras::get_avatar_url(msg->author);
                }
            }
            channel->send_embed(embed);
        });
    }


public:
    Help() {
        using namespace CDL;
        // Commands
        register_command("help", help, 1);
    }
};




static Help help;
